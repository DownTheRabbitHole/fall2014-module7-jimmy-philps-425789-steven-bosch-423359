'use strict';

/* Controllers */

var phonecatControllers = angular.module('phonecatControllers', []);

phonecatControllers.controller('PhoneListCtrl', ['$scope', 'Phone',
  function($scope, Phone) {
    Phone.query(function(phones){
      $scope.phones = phones;
      obtainPhones(Phone, $scope, phones.length, 0);
    });
    $scope.orderProp = 'age';
  }]);

var obtainPhones = function(Phone, scope, length, itr){
if (itr < length){
var  thisPhone = scope.phones[itr];   
Phone.get({phoneId: thisPhone.id}, function(phone) {
	    thisPhone.additionalFeatures = phone.additionalFeatures;
            thisPhone.android = phone.android;
            thisPhone.availability = phone.availability;
            thisPhone.battery = phone.battery;
            thisPhone.camera = phone.camera;
            thisPhone.connectivity = phone.connectivity;
            thisPhone.description = phone.description;
            thisPhone.display = phone.display;
	    thisPhone.display.screenSize = parseFloat(thisPhone.display.screenSize);
            thisPhone.hardware = phone.hardware;
            thisPhone.images = phone.images;
            thisPhone.name = phone.name;
            thisPhone.sizeAndWeight = phone.sizeAndWeight;
            thisPhone.storage = phone.storage;
	    if (thisPhone.storage.ram.split("M") != ""){
	      thisPhone.storage.ram = parseFloat(thisPhone.storage.ram.split("M"));
	    }
	    obtainPhones(Phone, scope, length, itr +1);
	});
}else{
 return;
}
}

phonecatControllers.controller('PhoneCompareCtrl', ['$scope', '$routeParams', 'Phone',
  function($scope, $routeParams, Phone) {
	$scope.phone1 = Phone.get({phoneId: $routeParams.phone1Id});
	$scope.phone2 = Phone.get({phoneId: $routeParams.phone2Id});


  }]);
phonecatControllers.controller('PhoneDetailCtrl', ['$scope', '$routeParams', 'Phone',
  function($scope, $routeParams, Phone) {
    $scope.phone = Phone.get({phoneId: $routeParams.phoneId}, function(phone) {
      $scope.mainImageUrl = phone.images[0];
    });

    $scope.setImage = function(imageUrl) {
      $scope.mainImageUrl = imageUrl;
    }
  }]);
